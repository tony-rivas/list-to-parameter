#List to Parameter

written by Tony Rivas, 2011

takes a list and converts it to a paramterized form

this was originally a first hello world javascript program, but then needed a tool to take a list of 30 or so keys
and put them into a sql insert statement, keys were not always consecutive, i was an intern back then, there
was probably a better way to do this

using https://clipboardjs.com for the copy button

![Alt text](/assets/screenshots/demo_list-to-parameter.gif?raw=true "demo")

how to use:

1) open parameter.html with a browser and supply a list

example list:
apple
orange
banana
coconut

2) select your settings

Separator: separates each list item e.g. apple,orange, or apple;orange;

Enclose: wraps around each item e.g. 'apple' or "apple" or $apple$

Outermost: wraps the whole thing e.g. ('apple','orange') or <"apple","orange">

Leading character: pads characters to list item e.g. 0apple or %apple

Number of characters: use with Leading character e.g. 000apple or %%%apple

Append to front: similar to Append to front except you can put anything you want in front of the item e.g. www.apple

Exclude from enclose?: use with Append to front, put chars outside of the enclose e.g. www.'apple'

Append to end: same as append to front but for the end of the item e.g. apple.com

Exclude from enclose? use with Append to enclose e.g. 'apple'.com

3) click Paramterize

4) copy everything out of the Results field or click "Copy to clipboard"

5) Clear List if you want to start over

6) Reset if you want to clear the list and wipe out the settings back to default
